import java.awt.Color;


public interface ConsolidatorStatics {
	public final static String WINDOW_NAME = "Consolidator v1.3";
	public final static String START_BUTTON_NAME = "START";
	public final static String STOP_BUTTON_NAME = "STOP";
	public final static String DEFAULT_SOURCE_PATH = "";
	public final static String DEFAULT_RESULT_PATH = "";
	public final static String DEFAULT_FILTER = "sql";
	public final static boolean INERRUPTED = true;
	public final static boolean NOT_INERRUPTED = false;
	public final static String RESULT_PATH_ERROR1 = "Result path not set!";
	public final static String SOURCE_PATH_ERROR1 = "Source path not set or unable to find given directory!";
	public final static long MAX_STRING_SIZE = 50000;
	public final static String OPER_STOPPED =	"Operation stopped";
	public final static String OPER_SUCCESFUL =	"Operation succesful";
	public final static String SOURCE_LABEL = "Source Dir:";
	public final static String RESULT_LABEL = "Result File:";
	public final static String FILTER_LABEL = "File Filter:";
	public final static Color COLOR_NOT_OK = new Color(178, 34, 34);
	public final static Color COLOR_OK = new Color(34, 139, 34);
	public final static String FILTER_SEPARATOR = ",";
	public final static String MAINPAT = "[a-zA-Z0-9]+";
	public final static int OUTPUT_CONSOLE = 1;
	public final static int OUTPUT_GUI = 2;
}

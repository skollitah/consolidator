import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Consolidator {
	private JFrame mainWin;
	private JTextArea text;
	private JPanel paths;
	private JPanel pathsLabels;
	private JPanel pathsStatus;
	private JButton statusSrc;
	private JButton statusRsl;
	private JButton statusFilter;
	private JPanel pathsText;
	private JTextField sourcePath;
	private JTextField resultPath;
	private JTextField filterText;
	private JPanel buttons;
	private JButton start;
	private JButton stop;
	private JScrollPane scroll;
	private Container cont;
	private List<String> extentionsList = new ArrayList<String>();
	private boolean interruptedFlag = ConsolidatorStatics.NOT_INERRUPTED;
	private boolean isSourcePathOk = false;
	private boolean isResultPathOk = false;
	private boolean isFilterOk = true;
	
	public static void main(String[] args) {
		if (args.length > 0) {
			new Consolidator(args);
		} else {
			new Consolidator();
		}
	}
	
	public Consolidator(String[] args) {
		if (args.length == 3) {
			if(checkSrc(args[0]) && checkRsl(args[1]) && checkFilter(args[2], extentionsList)) {
				consolidate(args[0], args[1], ConsolidatorStatics.OUTPUT_CONSOLE);
			}
			else {
				System.out.println("Incorrect values of arguments.");
			}
		} else {
			System.out.println("Incorrect number of arguments.");
		}
	}

	public Consolidator() {
		mainWin = new JFrame(ConsolidatorStatics.WINDOW_NAME);
		text = new JTextArea();
		paths = new JPanel();
		pathsLabels = new JPanel();
		pathsStatus = new JPanel();
		statusSrc = new JButton();
		statusRsl = new JButton();
		statusFilter = new JButton();
		pathsText = new JPanel();
		sourcePath = new JTextField(ConsolidatorStatics.DEFAULT_SOURCE_PATH);
		resultPath = new JTextField(ConsolidatorStatics.DEFAULT_RESULT_PATH);
		filterText = new JTextField(ConsolidatorStatics.DEFAULT_FILTER);
		buttons= new JPanel();
		start = new JButton(ConsolidatorStatics.START_BUTTON_NAME);
		stop = new JButton(ConsolidatorStatics.STOP_BUTTON_NAME);
		scroll = new JScrollPane();		
		paths.setLayout(new BorderLayout());
		pathsText.setLayout(new GridLayout(3, 1));
		pathsLabels.setLayout(new GridLayout(3, 1));
		pathsLabels.add(new JLabel(ConsolidatorStatics.SOURCE_LABEL));
		pathsLabels.add(new JLabel(ConsolidatorStatics.RESULT_LABEL));
		pathsLabels.add(new JLabel(ConsolidatorStatics.FILTER_LABEL));
		pathsStatus.setLayout(new GridLayout(3, 1));
		statusSrc.setEnabled(false);
		statusSrc.setBackground(ConsolidatorStatics.COLOR_NOT_OK);
		pathsStatus.add(statusSrc);
		statusRsl.setEnabled(false);
		statusRsl.setBackground(ConsolidatorStatics.COLOR_NOT_OK);
		pathsStatus.add(statusRsl);
		statusFilter.setEnabled(false);
		statusFilter.setBackground(ConsolidatorStatics.COLOR_OK);
		pathsStatus.add(statusFilter);
		paths.add(pathsLabels, BorderLayout.WEST);
		paths.add(pathsStatus, BorderLayout.EAST);		
		paths.add(pathsText, BorderLayout.CENTER);
		cont=mainWin.getContentPane();
		cont.setLayout(new BorderLayout());
		sourcePath.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}
			public void keyReleased(KeyEvent keyEvent) {
				isSourcePathOk = checkSrc(sourcePath.getText());
				if(isSourcePathOk) {
					statusSrc.setBackground(ConsolidatorStatics.COLOR_OK);
					if (isSourcePathOk && isResultPathOk && isFilterOk) {
						start.setEnabled(true);
					}					
				}
				else {
					statusSrc.setBackground(ConsolidatorStatics.COLOR_NOT_OK);
					start.setEnabled(false);
				}			
			}
			public void keyTyped(KeyEvent keyEvent) {
			}
		});
		resultPath.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}
			public void keyReleased(KeyEvent keyEvent) {
				isResultPathOk = checkRsl(resultPath.getText());
				if (isResultPathOk) {
					statusRsl.setBackground(ConsolidatorStatics.COLOR_OK);
					if (isSourcePathOk && isResultPathOk && isFilterOk) {	
						start.setEnabled(true);
					}
				}
				else {
					statusRsl.setBackground(ConsolidatorStatics.COLOR_NOT_OK);
					start.setEnabled(false);
				}
			}
			public void keyTyped(KeyEvent keyEvent) {
			}
		});
		filterText.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}
			public void keyReleased(KeyEvent keyEvent) {
				extentionsList.clear();
				isFilterOk = checkFilter(filterText.getText(), extentionsList);
				if (isFilterOk) {
					statusFilter.setBackground(ConsolidatorStatics.COLOR_OK);
					if (isSourcePathOk && isResultPathOk && isFilterOk) {	
						start.setEnabled(true);
					}
				}
				else {
					statusFilter.setBackground(ConsolidatorStatics.COLOR_NOT_OK);
					start.setEnabled(false);
				}						
			}
			public void keyTyped(KeyEvent keyEvent) {
			}		
		});
		checkFilter(filterText.getText(), extentionsList);
		pathsText.add(sourcePath);
		pathsText.add(resultPath);
		pathsText.add(filterText);
		scroll.createVerticalScrollBar();
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.createHorizontalScrollBar();
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		buttons.setLayout(new GridLayout(1, 2));
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					(new File(resultPath.getText())).createNewFile();
				}
				catch(IOException ex) {
					JOptionPane.showMessageDialog(mainWin, "Unable to create file: " + resultPath.getText(), "Error!", JOptionPane.ERROR_MESSAGE);
					return;
				}
				start.setEnabled(false);
				stop.setEnabled(true);
				Thread watek = new Thread() {
					public void run() {							
						consolidate(sourcePath.getText(), resultPath.getText(), ConsolidatorStatics.OUTPUT_GUI);
						isResultPathOk = checkRsl(resultPath.getText());
						if (isResultPathOk) {
							start.setEnabled(true);
							stop.setEnabled(false);
							statusRsl.setBackground(ConsolidatorStatics.COLOR_OK);
						} else {
							start.setEnabled(false);
							stop.setEnabled(false);
							statusRsl.setBackground(ConsolidatorStatics.COLOR_NOT_OK);
						}
					}
				};
				watek.start();
			}
		});
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				interruptedFlag = ConsolidatorStatics.INERRUPTED;
				start.setEnabled(true);
				stop.setEnabled(false);
			}
		});
		start.setEnabled(false);
		stop.setEnabled(false);
		buttons.add(start);
		buttons.add(stop);
		text.setEditable(false);
		cont.add(paths, BorderLayout.NORTH);
		scroll.getViewport().add(text);
		cont.add(scroll, BorderLayout.CENTER);
		cont.add(buttons, BorderLayout.SOUTH);
		mainWin.setSize(700, 300);
		mainWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainWin.setVisible(true);
	}

	private boolean checkFilter(String toParse, List<String> outputList) {
		if (toParse.equals("")) {
			return true;
		}
		String [] extentions = toParse.split(ConsolidatorStatics.FILTER_SEPARATOR);
		Pattern pattern=Pattern.compile(ConsolidatorStatics.MAINPAT);
		for(int i = 0; i< extentions.length; i++) {
			String tmpExt = extentions[i].trim();
			Matcher matcher = pattern.matcher(tmpExt);
			if (matcher.find() && matcher.group().compareTo(tmpExt)==0) {
				outputList.add(tmpExt);
				continue;
			}
			else {
				return false;
			}
		}
		return true;
	}

	private boolean checkSrc(String srcPath) {
		File sf = new File(srcPath);
		if (!srcPath.equals(ConsolidatorStatics.DEFAULT_SOURCE_PATH)
				&& sf.exists() && sf.isDirectory() && sf.canRead()) {
			return true;
		}
		else {
			return false;
		}
	}

	private boolean checkRsl(String rslPath) {
		File rf = new File(rslPath);
		if (!rslPath.equals(ConsolidatorStatics.DEFAULT_RESULT_PATH)) {
			if (rf.isFile() && rf.canWrite()) {
				return false;
			}
			else if ((new File(rf.getParent())).canWrite() && !rf.exists()) {
				return true;
			}
		}
		return false;
	}	

	private List<File> getFilesList(File startingPoint) {
		List<File> files = new ArrayList<File>();
		if (startingPoint.isDirectory()) {
			String[] children = startingPoint.list();
			for(int i = 0; i < children.length; i++) {
				File child = new File(startingPoint + File.separator + children[i]);
				if (child.isDirectory()) {
					files.addAll(getFilesList(child));
				}
				else if (child.isFile()) {
					files.add(child);
				}
			}
		}
		else if (startingPoint.isFile()) {
			files.add(startingPoint);
		}
		return files;
	}
	
	private List<File> filterFilesList(List<File> toBeFiltered, List<String> filters) {
		if(filters.size()==0) {
			return toBeFiltered;
		}
		List<File> filteredList = new ArrayList<File>();
		for (int i = 0; i < toBeFiltered.size(); i++) {
			for (int j = 0; j < filters.size(); j++) {
				if ((toBeFiltered.get(i)).getName().endsWith("." + filters.get(j))) {
					filteredList.add(toBeFiltered.get(i));
				}
			}
		}
		return filteredList;
	}
	
	public void consolidate(String srcPath, String rslFile, int outputType) {
		try	{		
			List<File> filteredList = filterFilesList(getFilesList(new File(srcPath)), extentionsList);
			int counter = 1;
			interruptedFlag=ConsolidatorStatics.NOT_INERRUPTED;
			String everything="";
			FileReader readfile;
			BufferedReader reader;
			FileWriter writefile=new FileWriter(rslFile, true);
			BufferedWriter writer=new BufferedWriter(writefile);
			for (int i = 0; i < filteredList.size(); i++) {
				String temp="";
				File tmpFile = filteredList.get(i);
				readfile=new FileReader(tmpFile);
				if (outputType == ConsolidatorStatics.OUTPUT_GUI) {
					text.append((counter++) + ". " + tmpFile.getPath() + "\n");
					cont.repaint();
				} else if (outputType == ConsolidatorStatics.OUTPUT_CONSOLE) {
					System.out.println((counter++) + ". " + tmpFile.getPath() + "\n");
				}
				reader=new BufferedReader(readfile);
				everything+="PRINT '" + tmpFile.getPath() + "'\r\n";
				everything+="go\r\n";					 
				while(reader.ready()) {
					if (interruptedFlag==ConsolidatorStatics.INERRUPTED) {
						readfile.close();
						writefile.close();
						return;
					}
					temp=reader.readLine();
					temp+="\r\n";
					everything+=temp;
					if(everything.length()>ConsolidatorStatics.MAX_STRING_SIZE) {
						writer.write(everything, 0, everything.length());
						writer.flush();
						everything="";
					}
				}
				everything+="go\r\n";
				writer.write(everything, 0, everything.length());
				writer.flush();
				everything="";
				readfile.close();
			}
			writefile.close();
			if (outputType == ConsolidatorStatics.OUTPUT_GUI) {
				text.append("SUCCESS!\n");
				cont.repaint();
			} else if (outputType == ConsolidatorStatics.OUTPUT_CONSOLE) {
				System.out.println("SUCCESS!\n");
			}
		} catch (Exception e) {
			System.out.println("Unexpected Error: " + e);
			System.exit(0);
		}
	}
}
